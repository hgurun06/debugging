package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.ILoan.LoanState;
import library.entities.helpers.BookHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class OverdueTestForBug2 {
	IBook book;
	IPatron patron;
	ILibrary library;
	ILoan loan;
	@BeforeEach
	void setUp() throws Exception {
		book = new Book("Author","Title", "CallNo",1);
		patron = new Patron("LName", "FName", "Email", 0452040302, 1);
		library = new Library(new BookHelper(), new PatronHelper(), new LoanHelper());
	}

	@AfterEach
	void tearDown() throws Exception {
	} 

	@Test
	void testCalculateOverDueFine() {
		//arrange
		int loanperiod = -2;
		Date dueDate = Calendar.getInstance().getDueDate(loanperiod);
		loan = new Loan(book, patron, dueDate , LoanState.OVER_DUE, 1);
		double expected = 1.0;
		//act 
		double actual = library.calculateOverDueFine(loan);
		//assert
		assertEquals(expected, actual);
		
	}

}
